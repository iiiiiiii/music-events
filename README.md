# Music events

A solution for responsive music events dashboard implementation.

## Setup

```.env``` should be created with API server information:
```
API_SERVER=https://api.server.for.the.app.local/
API_COUNTRY=EU
API_CLASSIFICATION_ID=classification_id
API_KEY=secret_api_key
```

## Install

```npm install```

## Run

### Development

```npm run start ```

### Build

```npm run build```

## Test

```npm run test```
