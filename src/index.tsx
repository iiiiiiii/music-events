import React from 'react';
import ReactDOM from 'react-dom';
import Dashboard from './components/dashboard';
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux';
import store, { persistor } from './app/store';

import './index.scss';

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <Dashboard />
            </PersistGate>
        </Provider>
    </React.StrictMode>,
    document.querySelector('body')
);
