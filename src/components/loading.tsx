import React from 'react';

export function Loading () {
    return (
        <section className="loading">
            <div className="loading__icon">
                <div className="loading__icon-spinner"></div>
            </div>
        </section>
    );
}
