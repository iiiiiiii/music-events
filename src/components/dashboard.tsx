import React, { useState, useEffect } from 'react';
import { getGenres, getEvents, getEventsByGenre } from '../api';
import store from '../app/store';
import Events from './events';
import Footer from './footer';
import Header from './header';
import Navigation from './navigation';
import Searchbar from './searchbar';

export default function Dashboard () {
    const state = store.getState();
    const [genre, setGenre] = useState('all');
    const [genres, setGenres] = useState(state.genres.items);
    const [events, setEvents] = useState(state.events.items);
    const [loading, setLoading] = useState(false);

    const loadGenres = () => {
        if (state.genres.updated && new Date().getTime() - state.genres.updated <= 3600000) {
            return;
        }

        setLoading(true);

        getGenres().then(something => {
            setLoading(false);

            if ("error" in something) {
                return;
            }

            store.dispatch({ type: 'add genres', item: (something.segment._embedded.genres as []) });

            setGenres(something.segment._embedded.genres);

            if (typeof state.genres.selected.id === 'undefined') {
                store.dispatch({ type: 'set selected genre', item: { id: 'all' } });
            }
        });
    };

    const loadEvents = () => {
        !state.genres.selected.id || state.genres.selected.id === 'all' ? loadAllEvents() : loadGenreEvents(state.genres.selected.id);
    };

    const loadAllEvents = () => {
        if (state.events.genreId === 'all' && state.events.updated && new Date().getTime() - state.events.updated <= 300000) {
            return;
        }

        setLoading(true);

        getEvents().then(something => {
            setLoading(false);
            
            if ("error" in something) {
                store.dispatch({ type: 'add events', item: [], genreId: genre });

                setEvents([]);

                return;
            }

            store.dispatch({ type: 'add events', item: (something._embedded.events as []), genreId: 'all' });

            setEvents(something._embedded.events);
        });
    };

    const loadGenreEvents = (genre: string) => {
        if (state.events.genreId === genre && state.events.updated && new Date().getTime() - state.events.updated <= 300000) {
            return;
        }
        setLoading(true);

        getEventsByGenre(genre).then(something => {
            setLoading(false);

            if ("error" in something) {
                store.dispatch({ type: 'add events', item: [], genreId: genre });

                setEvents([]);

                return;
            }

            store.dispatch({ type: 'add events', item: (something._embedded.events as []), genreId: genre });

            setEvents(something._embedded.events);
        });
    };


    const navigate = (destination: string) => {
        store.dispatch({ type: 'set selected genre', item: { id: destination }});
        setGenre(destination);
        loadEvents();
    };

    const filter = (input: string) => input === '' ? setEvents(state.events.items) : setEvents(events.filter(event => event.name.toLowerCase().includes(input.toLowerCase())));

    useEffect(() => {
        loadEvents();
        loadGenres();
    }, [genre]);

    return <>
        <Header>
            <Searchbar change={filter} />
            <Navigation genres={state.genres} navigate={navigate} />
        </Header>
        <Events data={events} loading={loading} />
        <Footer />
    </>;
}
