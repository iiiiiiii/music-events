import React, { ReactNode } from 'react';

type Header = {
    children?: ReactNode | undefined;
}

export default function Header (props: Header) {
    return (
        <header className="header">
            <h1 className="header__heading">Music events</h1>
            {props.children}
        </header>
    );
}