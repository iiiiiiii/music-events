import React, { ReactNode } from 'react';
import SearchIcon from '../images/search.svg';

type Searchbar = {
    change: Function
    children?: ReactNode | undefined;
}

export default function Searchbar (props: Searchbar) {
    const change = function (input: string) {
        props.change(input);
    };

    return (
        <section className="searchbar">
            <SearchIcon />
            <input className="searchbar__input" type="text" placeholder="Search for events..." onChange={(e) => change(e.target.value)} />
        </section>
    );
}
