import React, { ReactNode } from 'react';

type Footer = {
    children?: ReactNode | undefined;
}

export default function Footer (props: Footer) {
    return (
        <footer className="footer">
            <div>
                Fitek 2020
            </div>
            {props.children}
        </footer>
    );
}
