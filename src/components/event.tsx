import React, { ReactNode } from 'react';
import { Event as EventType } from '../api/types';

type Event = {
    data: EventType;
    select: Function;
    selected: string;
    children?: ReactNode | undefined;
}

export default function Event (props: Event) {
    const select = (id: string) => {
        props.select(id);
    }

    return (
        <figure className="event" data-selected={props.selected === props.data.id ? 'yes' : 'no'}>
            <a className="event__link" onClick={() => select(props.data.id)}>
                <img className="event__image" src={props.data.images.find(image => image.ratio == '4_3')?.url} alt={props.data.name} />
            </a>
            {props.children}
        </figure>
    );
}
