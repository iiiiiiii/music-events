import React, { useState, useEffect, useRef } from 'react';
import { Genres } from '../api/types';
import { throttle } from 'throttle-typescript';

type Navigation = {
    genres: Genres;
    navigate: Function;
}

const measuere = (el: HTMLElement) => parseFloat(window.getComputedStyle(el, null).width.replace('px', ''));

export default function Navigation (props: Navigation) {
    const [menu, setMenu] = useState({
        visible: [] as typeof props.genres.items,
        invisible: [] as typeof props.genres.items,
        accum: 0
    });
    const navRef = useRef<HTMLElement>(null);
    const visibleRef = useRef<HTMLUListElement>(null);
    const invisibleRef = useRef<HTMLUListElement>(null);

    const updateNavTop = (nav: HTMLElement, visible: HTMLUListElement, invisible: HTMLUListElement) => {
        if (!invisible || invisible.offsetHeight <= 0) {
            return;
        }
        
        invisible.style.top = (window.scrollY < (nav.offsetTop + nav.offsetHeight) ? (nav.offsetTop + nav.offsetHeight) - window.scrollY <= 0 ? 0 : ((nav.offsetTop + nav.offsetHeight) - window.scrollY) : (nav.offsetTop + nav.offsetHeight)) + 'px';
    };

    const click = () => {
        if (navRef.current === null || visibleRef.current === null || invisibleRef.current === null) {
            return;
        }
        updateNavTop(navRef.current, visibleRef.current, invisibleRef.current);
    };

    if (menu.visible.length <= 0 && menu.invisible.length <= 0 && props.genres.items.length > 0) {
        setMenu({
            visible: props.genres.items,
            invisible: [],
            accum: 0
        });
    }

    useEffect(() => {
        const nav = navRef.current as HTMLElement;
        const visible = visibleRef.current as HTMLUListElement;
        const invisible = invisibleRef.current as HTMLUListElement;

        const updateNav = () => {
            if (menu.accum >= props.genres.items.length) {
                return;
            }

            const navWidth = measuere(nav);
            const visibleWidth = measuere(visible);
            const availableSpace = navWidth - visibleWidth - 50;
            
            if (visibleWidth > navWidth) {
                setMenu({
                    visible: menu.visible.slice(0, menu.visible.length - 1),
                    invisible: [...menu.visible.slice(menu.visible.length - 1, menu.visible.length), ...menu.invisible],
                    accum: menu.accum + 1
                })
            }

            if (availableSpace > 200) {
                setMenu({
                    visible: [...menu.visible, ...menu.invisible.slice(0, 1)],
                    invisible: menu.invisible.slice(1, menu.invisible.length),
                    accum: menu.accum + 1
                })
            }
        };

        const handleUpdateNavTop = () => {
            updateNavTop(nav, visible, invisible);
        };

        window.addEventListener('resize', throttle(updateNav, 100));

        window.addEventListener('scroll', handleUpdateNavTop);

        updateNav();

    }, [menu, props.genres.items]);

    const navigate = (destination: string) => {
        props.navigate(destination);
    };

    return (
        <nav className="navigation" ref={navRef}>
            <ul className="navigation__list navigation__list--visible" ref={visibleRef}>
                <li className="navigation__item">
                    <a className={'navigation__link' + (props.genres.selected.id === 'all' ? ' navigation__link--active' : '')} onClick={() => navigate('all')}>
                        All genres
                    </a>
                </li>
                {
                    menu.visible.map(genre => (
                        <li key={genre.id} className="navigation__item">
                            <a onClick={() => navigate(genre.id)} className={'navigation__link' + (props.genres.selected.id === genre.id ? ' navigation__link--active' : '')}>
                                {genre.name}
                            </a>
                        </li>
                    ))
                } 
                {
                    menu.invisible.length > 0 ?
                        <li className="navigation__item navigation__item--more">
                            <a className="navigation__link" onMouseEnter={click}>
                                More...
                            </a>
                            <ul className="navigation__list navigation__list--invisible" ref={invisibleRef}>
                                {menu.invisible.map(genre => <li className="navigation__item" key={genre.id}>
                                    <a onClick={() => navigate(genre.id)} className={'navigation__link' + (props.genres.selected.id === genre.id ? ' navigation__link--active' : '')}>
                                        {genre.name}
                                    </a>
                                </li>)} 
                            </ul>
                        </li> 
                    : ''
                }
            </ul>
        </nav>
    );
}