import React, { useState, useEffect } from 'react';
import { getEvent } from '../api';
import { EventDetails as EventDetailsType, Event as EventType } from '../api/types';
import Event from './event';
import EventDetails from './eventdetails';
import { Loading } from './loading';

type Events = {
    data: Array<EventType>;
    loading: boolean;
};

export default function Events (props: Events) {
    const [selected, setSelected] = useState('');
    const [event, setEvent] = useState<EventDetailsType>()
    const select = (id: string) => {
        setSelected(id);
    }
    
    useEffect(() => {
        if (selected === '') {
            return;
        }

        getEvent(selected).then(something => {
            if ("error" in something) {
                return;
            }

            setEvent(something as EventDetailsType);
        })
    }, [selected]);

    return (
        props.loading ?
            <Loading />
        :
            <section className="events">
                {
                    props.data.map((item) => (
                        <Event key={item.id} data={item} selected={selected} select={select}>
                            {
                                selected === item.id && event ?
                                    <EventDetails data={event as EventDetailsType} />
                                : ''
                            }
                        </Event>
                    ))
                }
            </section>
    );
}
