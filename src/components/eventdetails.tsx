import React from 'react';
import { EventDetails } from '../api/types';
import CalendarIcon from '../images/calendar.svg'
import VenueIcon from '../images/venue.svg'
import { format } from '../app/help';

type Event = {
    data: EventDetails;
};

export default function EventDetails (props: Event) {
    return (
        <figcaption className="eventdetails">
            <figcaption className="eventdetails__details">
                <h2 className="eventdetails__heading">{props.data.name}</h2>
                <ul className="eventdetails__list">
                    <li className="eventdetails__list-item">
                        <CalendarIcon /> {format(new Date(props.data.dates.start.dateTime))}
                    </li>
                    <li className="eventdetails__list-item">
                        <VenueIcon /> {props.data.classifications[0].genre.name}
                    </li>
                </ul>
                <p>
                    <a href={props.data.url}>{props.data.url}</a>
                </p>
            </figcaption>
            <figure className="eventdetails__figure">
                <img className="eventdetails__image" src={props.data.images.find((image) => image.ratio === '4_3')?.url} alt={props.data.name} />
            </figure>
        </figcaption>
    );
}
