import React from 'react';
import { render, screen, waitFor, cleanup } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import '@testing-library/jest-dom';
import * as api from '../../api';
import Dashboard from '../dashboard';
import store from '../../app/store';
import 'window-resizeto/polyfill';


const mocked = {
  events: {
    _embedded: {
      events: [{
        name: 'Mock Fest 2021',
        id: 'mockfest',
        images: [{
          ratio: '4_3',
          url: 'https://via.placeholder.com/161x119/FEC017/252B40'
        }],
        dates: {
          start: {
            dateTime: '2011-08-12T20:17:46.384Z'
          }
        },
        classifications: [{
          genre: {
            name: 'Mick-Mock'
          }
        }],
        url: 'http://www.example.com/'
      },
      {
        name: 'Fest Mock 2021',
        id: 'festmock',
        images: [],
        dates: {
          start: {
            dateTime: '2012-08-12T20:17:46.384Z'
          }
        },
        classifications: [{
          genre: {
            name: 'Mock-Mick'
          }
        }],
        url: 'http://www.example.com/'
      },
      {
        name: 'Elit Aenean Ornare',
        id: 'elitaenanornare',
        images: [{
          ratio: '4_3',
          url: 'https://via.placeholder.com/161x119/000000/252B40'
        }],
        dates: {
          start: {
            dateTime: '2017-08-12T20:17:46.384Z'
          }
        },
        classifications: [{
          genre: {
            name: 'Mock-Mick'
          }
        }],
        url: 'http://www.example.com/'
      }]
    }
  },
  eventsByGenre: {
    _embedded: {
        events: [{
          name: 'Elit Aenean Ornare',
          id: 'elitaenanornare',
          images: [{
            ratio: '4_3',
            url: 'https://via.placeholder.com/161x119/000000/252B40'
          }],
          dates: {
            start: {
              dateTime: '2017-08-12T20:17:46.384Z'
            }
          },
          classifications: [{
            genre: {
              name: 'Mock-Mick'
            }
          }],
          url: 'http://www.example.com/'
      },
      {
        name: 'Bibendum Condimentum Tellus Purus',
        id: 'bibendumcondimentum',
        images: [{
          ratio: '4_3',
          url: 'https://via.placeholder.com/161x119/FFFFFF/CC0000'
        }],
        dates: {
          start: {
            dateTime: '2019-01-02T20:17:46.384Z'
          }
        },
        classifications: [{
          genre: {
            name: 'Mock-Mick'
          }
        }],
        url: 'http://www.example.com/'
    }]
    }
  },
  event: {
      name: 'Mock Fest 2021',
      id: 'mockfest',
      images: [{
        ratio: '4_3',
        url: 'https://via.placeholder.com/161x119/FEC017/252B40'
      }],
      dates: {
        start: {
          dateTime: '2011-08-12T20:17:46.384Z'
        }
      },
      classifications: [{
        genre: {
          name: 'Mick-Mock'
        }
      }],
      url: 'http://www.example.com/'
  },
  event2: {
    name: 'Fest Mock 2021',
    id: 'festmock',
    images: [],
    dates: {
      start: {
        dateTime: '2012-08-12T20:17:46.384Z'
      }
    },
    classifications: [{
      genre: {
        name: 'Mock-Mick'
      }
    }],
    url: 'http://www.example.com/'
  },
  genres: {
      segment: {
          _embedded: {
              genres: [{
                name: 'Mick-Mock',
                id: 'mickmock'
            },
            {
              name: 'Mock Mick',
              id: 'mockmick'
            },
            {
              name: 'Mack Muck',
              id: 'mackmuck'
            },
            {
              name: 'Mack Buk',
              id: 'mackbuk'
            },
            {
              name: 'Lok Luk',
              id: 'lokluk'
            },
            {
              name: 'Lik Lok',
              id: 'likloc'
            },
            {
              name: 'Tik Doc',
              id: 'tikdoc'
            },
            {
              name: 'Pit Top',
              id: 'pittop'
            },
            {
              name: 'Bill Bull',
              id: 'billbull'
            },
            {
              name: 'Tam Tum',
              id: 'tamtum'
            },
            {
              name: 'Tim Tum',
              id: 'timtum'
            },
            {
              name: 'Fim Fom',
              id: 'fimfom'
            },
            {
              name: 'Din Don',
              id: 'dindon'
            },
            {
              name: 'Bam Bum',
              id: 'bambum'
            },
            {
              name: 'Pin Pon',
              id: 'pinpon'
            }]
          }
      }
  },
  rejection: {
    error: 'API unresponsive'
  }
};

/*
const getGenres = new Promise((resolve, reject) => {
  resolve(mocked.genres);
});
*/

afterEach(() => {
  jest.clearAllMocks();
  jest.resetAllMocks();
  jest.restoreAllMocks();

  localStorage.clear();

  store.dispatch({ type: 'reset state' });

  document.body.innerHTML = '';
});

describe('<Dashboard />', () => {
  it('works', async () => {
    jest.spyOn(api, 'getGenres').mockImplementationOnce(() => new Promise(resolve => resolve(mocked.genres)));
    jest.spyOn(api, 'getEvents').mockImplementationOnce(() => new Promise(resolve => resolve(mocked.events)));

    render(<Dashboard />);
    
    await waitFor(() => {
      expect(document.querySelector('.navigation__list')).toBeInTheDocument();
    });

    await waitFor(() => {
      expect(document.querySelector('.events')).toBeInTheDocument();
      expect(document.querySelector('.event__link')).toBeInTheDocument();

      jest.spyOn(api, 'getEvent').mockImplementationOnce(() => new Promise(resolve => resolve(mocked.event)));

      userEvent.click(document.querySelectorAll('.event__link')[0] as HTMLAnchorElement);
    });

    await waitFor(() => {
      expect(document.querySelector('.eventdetails')).toBeInTheDocument();

      jest.spyOn(api, 'getEvent').mockImplementationOnce(() => new Promise(resolve => resolve(mocked.event2)));

      userEvent.click(document.querySelectorAll('.event__link')[1] as HTMLAnchorElement);
    });

    await waitFor(() => {
      expect(document.querySelectorAll('.navigation__item')[1]).toBeInTheDocument();

      jest.spyOn(api, 'getEventsByGenre').mockImplementationOnce(() => new Promise(resolve => resolve(mocked.eventsByGenre)));

      userEvent.click(document.querySelectorAll('.navigation__item')[1].querySelector('.navigation__link') as HTMLAnchorElement);
    });

    await waitFor(() => {
      expect(document.querySelector('.eventdetails')).not.toBeInTheDocument();

      userEvent.type(document.querySelector('.searchbar__input') as HTMLInputElement, 'fest');

      expect(document.querySelector('.event')).toBeInTheDocument();
      
      jest.spyOn(api, 'getEvents').mockImplementationOnce(() => new Promise(resolve => resolve(mocked.events)));

      userEvent.click(document.querySelectorAll('.navigation__item')[0].querySelector('.navigation__link') as HTMLAnchorElement);
    });

    await waitFor(() => {
      const input = document.querySelector('.searchbar__input') as HTMLInputElement;

      userEvent.type(input, 'quamipsum');

      expect(document.querySelector('.event')).not.toBeInTheDocument();

      input.setSelectionRange(0, input.value.length);

      userEvent.type(input, '{backspace}');
    });

    await waitFor(() => {
      expect(document.querySelector('.event')).toBeInTheDocument();
    });
  });

  it('catches errors on genre/events load', async () => {
    jest.spyOn(api, 'getGenres').mockImplementation(() => new Promise(resolve => resolve(mocked.rejection)));
    jest.spyOn(api, 'getEvents').mockImplementation(() => new Promise(resolve => resolve(mocked.rejection)));

    render(<Dashboard />);
    
    await waitFor(() => {
      expect(document.querySelector('.events')).not.toBeInTheDocument();
    });
  });

  it('catches errors on event load', async () => {
    jest.spyOn(api, 'getGenres').mockImplementation(() => new Promise(resolve => resolve(mocked.genres)));
    jest.spyOn(api, 'getEvents').mockImplementation(() => new Promise(resolve => resolve(mocked.events)));
    jest.spyOn(api, 'getEvent').mockImplementation(() => new Promise(resolve => resolve(mocked.rejection)));

    render(<Dashboard />);

    await waitFor(() => {
      expect(document.querySelector('.events')).toBeInTheDocument();
      expect(document.querySelector('.event__link')).toBeInTheDocument();

      userEvent.click(document.querySelector('.event__link') as HTMLAnchorElement);
    });

    await waitFor(() => {
      expect(document.querySelector('.eventdetails')).not.toBeInTheDocument();
    });
  });

  it('catches errors on event load', async () => {
    jest.spyOn(api, 'getGenres').mockImplementation(() => new Promise(resolve => resolve(mocked.genres)));
    jest.spyOn(api, 'getEvents').mockImplementation(() => new Promise(resolve => resolve(mocked.events)));
    jest.spyOn(api, 'getEventsByGenre').mockImplementationOnce(() => new Promise(resolve => resolve(mocked.rejection)));

    render(<Dashboard />);

    await waitFor(() => {
      expect(document.querySelector('.event')).toBeInTheDocument();
      expect(document.querySelectorAll('.navigation__item')[1]).toBeInTheDocument();

      userEvent.click(document.querySelectorAll('.navigation__item')[1].querySelector('.navigation__link') as HTMLAnchorElement);
    });

    await waitFor(() => {
      expect(document.querySelector('.event')).not.toBeInTheDocument();
    });
  });
});
