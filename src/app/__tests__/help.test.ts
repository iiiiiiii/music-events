import * as help from '../help';

describe('pad', () => {
    it('should led 2 zeroes by default', () => {
        expect(help.pad(1)).toBe('01');
        expect(help.pad(0)).toBe('00');
        expect(help.pad(100)).toBe('100');
    });

    it('should be configurable size', () => {
        expect(help.pad(1, 4)).toBe('0001');
        expect(help.pad(0, 1)).toBe('0');
        expect(help.pad(100, 7)).toBe('0000100');
    });
});

describe('format', () => {
    it('should take date object and reformat it to string', () => {
        const date = new Date('2011-08-12T20:17:46.384Z');

        expect(help.format(date)).toBe('Friday, 12.8.2011 @ 23:17');
    });
});
