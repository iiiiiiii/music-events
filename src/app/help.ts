export const pad = (number: number, size: number = 2) => {
    let result: string = number.toString();

    while (result.length < size) result = '0' + result;

    return result;
}

export const week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

export const format = (date: Date) => week[date.getDay()] + ', ' + date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear() + ' @ ' + pad(date.getHours()) + ':' + pad(date.getMinutes());
