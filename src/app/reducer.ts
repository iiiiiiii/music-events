import { State, Action, Event } from '../api/types';

const empty: State = {
    events: {
        selected: {} as Event,
        items: [],
        updated: 0,
        genreId: 'all',
    },
    genres: {
        selected: {} as Event,
        items: [],
        updated: 0,
    },
};

const reducer = (state: State = empty, action: Action) => {
    switch (action.type) {
        case 'add events':
            return {
                ...state,
                events: {
                    ...state.events,
                    items: action.item,
                    updated: new Date().getTime(),
                    genreId: action.genreId,
                }
            };

        case 'add genres':
            return {
                ...state,
                genres: {
                    ...state.genres,
                    items: action.item,
                    updated: new Date().getTime(),
                }
            };

        case 'set selected genre':
            return {
                ...state,
                genres: {
                    ...state.genres,
                    selected: action.item,
                }
            };   
            
        case 'reset state':
            return empty;

        default:
            return state
    }
};

export default reducer;
