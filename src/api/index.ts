import { root, countryCode, classificationId, apikey } from '../../config';

export const request = (entry: string, obj = {}) => root + entry + '.json?' + new URLSearchParams({ ...obj, apikey}).toString();

export type Something = {
    [key: string]: any;
};

export async function getEvents (): Promise<Something> {
    try {
        const response = await fetch(request('events', { classificationId, countryCode }));
    
        return response.json();
    } catch (e) {
        return {
            error: 'API unresponsive'
        }
    }
}

export async function getEventsByGenre (classificationId: string): Promise<Something> {
    try {
        const response = await fetch(request('events', { classificationId }));
    
        return response.json();
    } catch (e) {
        return {
            error: 'API unresponsive'
        }
    }
}

export async function getEvent (id: string): Promise<Something> {
    try {
        const response = await fetch(request('events/' + id));
    
        return response.json();
    } catch (e) {
        return {
            error: 'API unresponsive'
        }
    }
}

export async function getGenres (): Promise<Something> {
    try {
        const response = await fetch(request('classifications/' + classificationId));
    
        return response.json();
    } catch (e) {
        return {
            error: 'API unresponsive'
        }
    }
}
