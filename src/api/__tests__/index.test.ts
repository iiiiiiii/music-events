import fetch from 'jest-fetch-mock';
import * as api from '../index';

const mocked = {
    events: {
        _embedded: {
            events: [{
                name: 'Mock Fest 2021'
            }]
        }
    },
    event: {
        name: "Mock Fest 2021",
    },
    genres: {
        segment: {
            _embedded: {
                genres: [{
                    name: 'Mick-Mock'
                }]
            }
        }
    }
};

fetch.enableMocks();

beforeEach(() => {
    fetch.resetMocks();
});

describe('getting all events', () => {
    it("returns data", async () => {
        fetch.mockResponseOnce(JSON.stringify(mocked.events));

        const something = await api.getEvents();

        expect(something._embedded.events[0].name).toEqual('Mock Fest 2021');
        expect(fetch).toHaveBeenCalledTimes(1);
        expect(fetch).toHaveBeenCalledWith(
            'http://localhost/events.json?classificationId=&countryCode=&apikey='
        );
    });

    it("returns error message when exception", async () => {
        fetch.mockReject(() => Promise.reject());
      
        const something = await api.getEvents();
      
        expect(something).toEqual({"error": "API unresponsive"});
        expect(fetch).toHaveBeenCalledTimes(1);
        expect(fetch).toHaveBeenCalledWith(
          'http://localhost/events.json?classificationId=&countryCode=&apikey='
        );
      });
});

describe('getting events by genre', () => {
    it("returns data", async () => {
        fetch.mockResponseOnce(JSON.stringify(mocked.events));

        const genre = 'mickmock';
        const something = await api.getEventsByGenre(genre);

        expect(something._embedded.events[0].name).toEqual('Mock Fest 2021');
        expect(fetch).toHaveBeenCalledTimes(1);
        expect(fetch).toHaveBeenCalledWith(
            'http://localhost/events.json?classificationId=' + genre +'&apikey='
        );
    });

    it("returns error message when exception", async () => {
        fetch.mockReject(() => Promise.reject());
      
        const genre = 'mickmock';
        const something = await api.getEventsByGenre(genre);
      
        expect(something).toEqual({"error": "API unresponsive"});
        expect(fetch).toHaveBeenCalledTimes(1);
        expect(fetch).toHaveBeenCalledWith(
          'http://localhost/events.json?classificationId=' + genre +'&apikey='
        );
      });
});

describe('getting single event', () => {
    it("returns data", async () => {
        fetch.mockResponseOnce(JSON.stringify(mocked.event));

        const id = 'mockfest';
        const something = await api.getEvent(id);

        expect(something.name).toEqual('Mock Fest 2021');
        expect(fetch).toHaveBeenCalledTimes(1);
        expect(fetch).toHaveBeenCalledWith(
            'http://localhost/events/' + id + '.json?apikey='
        );
    });

    it("returns error message when exception", async () => {
        fetch.mockReject(() => Promise.reject());
      
        const id = 'mockfest';
        const something = await api.getEvent(id);

        expect(something).toEqual({"error": "API unresponsive"});
        expect(fetch).toHaveBeenCalledTimes(1);
        expect(fetch).toHaveBeenCalledWith(
          'http://localhost/events/' + id + '.json?apikey='
        );
      });
});

describe('getting genres', () => {
    it("returns data", async () => {
        fetch.mockResponseOnce(JSON.stringify(mocked.genres));

        const something = await api.getGenres();

        expect(Array.isArray(something.segment._embedded.genres)).toBe(true);
        expect(something.segment._embedded.genres[0].name).toEqual('Mick-Mock');
        expect(fetch).toHaveBeenCalledTimes(1);
        expect(fetch).toHaveBeenCalledWith(
            'http://localhost/classifications/.json?apikey='
        );
    });

    it("returns error message when exception", async () => {
        fetch.mockReject(() => Promise.reject());
      
        const events = await api.getGenres();
      
        expect(events).toEqual({"error": "API unresponsive"});
        expect(fetch).toHaveBeenCalledTimes(1);
        expect(fetch).toHaveBeenCalledWith(
          'http://localhost/classifications/.json?apikey='
        );
      });
});
