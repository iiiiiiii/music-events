export type State = {
    events: Events,
    genres: Genres,
};

export type Action = {
    type: string;
    item: any;
    genreId: string;
};

export type Events = {
    selected: {};
    items: Array<Event>;
    updated: number;
    genreId: string;
};

export type Genres = {
    selected: Event;
    items: Array<Genre>;
    updated: number;
};

export type Genre = {
    [key: string]: any;
};

export type Event = {
    classifications: Array<Classification>;
    dates: Dates,
    id: string;
    images: Array<Image>,
    locale: string,
    name: string,
    priceRanges: Array<PriceRange>,
    promoter: Promoter,
    promoters: Array<Promoter>;
    sales: Sales;
    test: Boolean;
    type: string;
    url: string;
};

type Classification = {
    family: boolean;
    genre: IdName,
    primary: Boolean,
    segment: IdName,
    subGenre: IdName
};

type Dates = {
    spanMultipleDays: boolean;
    start: StartDate;
    status: Status;
    timezone: string;
}

type StartDate = {
    dateTBA: Boolean;
    dateTBD: Boolean;
    dateTime: string;
    localDate: string;
    noSpecificTime: Boolean;
    timeTBA: Boolean;
};

type Status = {
    code: string;
};

type Promoter = IdName;

type Sales = {
    public: {
        endDateTime: string;
        startDateTime: string;
        startTBA: Boolean;
        startTBD: Boolean;
    }
}

type IdName = {
    id: string;
    name: string;
}

type Image = {
    ratio: string;
    width: number
    height: number;
    url: string;
    fallback: Boolean;
};

type PriceRange = {
    currency: string;
    max: number;
    min: number;
    type: string;
};

export type EventDetails = {
    _links: {
        self: {
            href: string;
            templated: boolean;
        },
        venues: Array<object>;
        attractions: Array<object>;
    };
    _embedded: {
        venues: Array<object>;
        attractions: Array<object>;
    };
    type: string;
    distance: number;
    units: string;
    location: {
        longtitude: number;
        latitude: number;
    };
    id: string;
    locale: string;
    name: string;
    description: string;
    additionalInfo: string;
    url: string;
    images: Array<Image>;
    dates: {
        start: {
            localDate: string;
            localTime: LocalTime;
            dateTime: string;
            dateTBD: boolean;
            dateTBA: boolean;
            timeTBA: boolean;
            noSpecificTime: boolean;
        };

        end: {
            localDate: string;
            localTime: LocalTime;
            dateTime: string;
            approximate: boolean;
            noSpecificTime: boolean;
        };
        access: {
            startDateTime: string;
            startApproximate: boolean;
            endDateTime: string;
            endApproximate: boolean;
        };
        timezone: string;
        status: {
            code: 'onsale' | 'offsale' | 'canceled' | 'postponed' | 'rescheduled'
        };
        spanMultipleDays: boolean;
    };
    sales: {
        public: {
            startDateTime: string;
            endDateTime: string;
            startTBD: boolean;
        };
        presales: [
            {
                name: string;
                description: string;
                url: string;
                startDateTime: string;
                endDateTime: string;
            }
        ]
    };
    info: string;
    pleaseNote: string;
    priceRanges: [
        {
            type: string;
            currency: string;
            min: number;
            max: number;
        }
    ];
    promoter: object;
    promoters: Array<Promoter>;
    outlets: [
        {
            url: string;
            type: string;
        }
    ];
    productType: string;
    products: Array<object>;
    seatmap: {
        staticUrl: string;
    };
    accessibility: {
        info: string;
    };
    ticketLimit: {
        info: string;
        infos: object
    };
    classifications: Array<Classification>;
    place: {
        area: {
            name: string;
        };
        address: {
            line1: string;
            line2: string;
            line3: string;
        };
        city: {
            name: string;
        };
        state: {
            stateCode: string;
            name: string;
        };
        country: {
            countryCode: string; // ISO 3166
            name: string;
        };
        postalCode: string;
        location: {
            longtitude: number;
            latitude: number;
        };
        name: string;
    };
    externalLinks: object;
    test: boolean;
    aliases: Array<string>;
    localizedAliases: object;
}

type LocalTime = {
    chronology: {
        zone: {
            fixed: boolean;
            id: string;
        }
    };
    millisOfSecond: number;
    millisOfDay: number;
    secondOfMinute: number;
    minuteOfHour: number;
    hourOfDay: number;
    values: Array<object>;
    fieldTypes: Array<object>;
    fields: Array<object>;
}
