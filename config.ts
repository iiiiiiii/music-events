export const root = process.env.API_SERVER || 'http://localhost/';
export const countryCode = process.env.API_COUNTRY || '';
export const classificationId = process.env.API_CLASSIFICATION_ID || '';
export const apikey = process.env.API_KEY || '';
